Pod::Spec.new do |s|

s.name         = "LLUnreadBadgeView"
s.version      = "1.0.3"
s.summary      = "LLUnreadBadgeView"
s.license      = { :type => "MIT", :file => "LICENSE" }

s.description  = <<-DESC
APP消息未读数小红点，支持拖拽
DESC

#提交到Gitee的私有空间（为啥不提交到GitHub，因为墙！😭）
s.homepage      = "https://gitee.com/timenode-wuhan/badgeview"
s.author        = { "春风十里，不如你" => "gonghongorzm@163.com" }
s.platform      = :ios, "11.0"
s.source        = { :git => "https://gitee.com/timenode-wuhan/badgeview.git", :tag => s.version }

#需要打包的文件
s.source_files = "BadgeView/*.{h,m}"

#为了同时支持真机和模拟器
s.pod_target_xcconfig = { 'VALID_ARCHS' => 'x86_64 armv7 arm64' }

#依赖的库  这里自行填写，不要遗漏
s.frameworks   = 'UIKit', 'Foundation'

# 需要的其他三方库
#s.dependency 'Masonry'

end

