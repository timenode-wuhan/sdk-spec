Pod::Spec.new do |s|

s.name         = "LiaoLiaoPermissionKit"
s.version      = "1.0.0"
s.summary      = "LiaoLiaoPermissionKit私有库"
s.license      = { :type => "MIT", :file => "LICENSE" }

s.description  = <<-DESC
LiaoLiaoPermissionKit 工厂，APP权限请求与管理
DESC

#提交到Gitee的私有空间（为啥不提交到GitHub，因为墙！😭）
s.homepage      = "https://e.gitee.com/timenode-wuhan/repos/timenode-wuhan/liaoliaopermissionkit/sources"
s.author        = { "春风十里，不如你" => "gonghongorzm@163.com" }
s.platform      = :ios, "11.0"
s.source        = { :git => "https://gitee.com/timenode-wuhan/liaoliaopermissionkit.git", :tag => s.version }

#需要打包的文件
s.source_files = "LiaoLiaoPermissionKit/*.{h,m}"

#依赖的库  这里自行填写，不要遗漏
s.frameworks   = 'UIKit', 'Foundation'

#为了同时支持真机和模拟器
s.pod_target_xcconfig = { 'VALID_ARCHS' => 'x86_64 armv7 arm64' }


end

