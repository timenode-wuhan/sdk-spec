Pod::Spec.new do |s|

s.name         = "LiaoLiaoMonitor"
s.version      = "1.0.0"
s.summary      = "LiaoLiaoMonitor"
s.license      = { :type => "MIT", :file => "LICENSE" }

s.description  = <<-DESC
APP功能监控管理插件，包含扩展、网络请求、APP监控等、如需使用，请仔细查看.h文件
DESC

s.homepage      = "https://gitee.com/time-node-wuhan-network/liaoliaomonitor"
s.author        = { "春风十里，不如你" => "gonghongorzm@163.com" }
s.platform      = :ios, "11.0"
s.source        = { :git => "https://gitee.com/time-node-wuhan-network/liaoliaomonitor.git", :tag => s.version }

s.vendored_frameworks = 'LiaoLiaoMonitor.framework'
s.pod_target_xcconfig = { 'VALID_ARCHS' => 'x86_64 armv7 arm64' }

# 需要的其他三方库
s.dependency 'SVProgressHUD'
s.dependency 'MJRefresh'
s.dependency 'QCloudCOSXML/Transfer'
s.dependency 'YYKit'
s.dependency 'SVGAPlayer'
s.dependency 'SSZipArchive'
s.dependency 'StreamingKit'
s.dependency 'AFNetworking'

end

