Pod::Spec.new do |s|

s.name         = "LiaoLiaoMonitor"
s.version      = "1.0.4"
s.summary      = "LiaoLiaoMonitor"
s.license      = { :type => "MIT", :file => "LICENSE" }

s.description  = <<-DESC
APP功能监控管理插件，包含扩展、网络请求、APP监控等、如需使用，请仔细查看.h文件
DESC

#提交到Gitee的私有空间（为啥不提交到GitHub，因为墙！😭）
s.homepage      = "https://gitee.com/timenode-wuhan/liaoliaomonitor"
s.author        = { "龚洪" => "gonghongorzm@163.com" }
s.platform      = :ios, "11.0"
s.source        = { :git => "https://gitee.com/timenode-wuhan/liaoliaomonitor.git", :tag => s.version }

s.vendored_frameworks = 'LiaoLiaoMonitor.framework'
s.pod_target_xcconfig = { 'VALID_ARCHS' => 'x86_64 armv7 arm64' }


end

