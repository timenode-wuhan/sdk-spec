Pod::Spec.new do |s|

s.name         = "LiaoLiaoLocalAudioManager"
s.version      = "1.0.0"
s.summary      = "LiaoLiaoLocalAudioManager 本地音频播放&&录制管理"
s.license      = { :type => "MIT", :file => "LICENSE" }

s.description  = <<-DESC
 LiaoLiaoLocalAudioManager，敏捷开发组件库，在正式使用前，请确保库更新！
DESC

#提交到Gitee的私有空间（为啥不提交到GitHub，因为墙！😭）
s.homepage      = "https://e.gitee.com/timenode-wuhan/repos/timenode-wuhan/liaoliaolocalaudiomanager/sources"
s.author        = { "春风十里，不如你" => "gonghongorzm@163.com" }
s.platform      = :ios, "11.0"
s.source        = { :git => "https://gitee.com/timenode-wuhan/liaoliaolocalaudiomanager.git", :tag => s.version }

#需要打包的文件
s.source_files = "LiaoLiaoLocalAudioManager/*.{h,m}"

#依赖的库  这里自行填写，不要遗漏
s.frameworks   = 'UIKit', 'Foundation'

#为了同时支持真机和模拟器
s.pod_target_xcconfig = { 'VALID_ARCHS' => 'x86_64 armv7 arm64' }


end

