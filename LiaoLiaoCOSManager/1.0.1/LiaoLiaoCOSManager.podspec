Pod::Spec.new do |s|

s.name         = "LiaoLiaoCOSManager"
s.version      = "1.0.1"
s.summary      = "LiaoLiaoCOSManager"
s.license      = { :type => "MIT", :file => "LICENSE" }

s.description  = <<-DESC
腾讯云COS对象存储SDK封装
DESC

s.homepage      = "https://gitee.com/timenode-wuhan/liaoliaocosmanager"
s.author        = {"春风十里，不如你" => "gonghongorzm@163.com"}
s.platform      = :ios, "11.0"
s.source        = {:git => "https://gitee.com/timenode-wuhan/liaoliaocosmanager.git", :tag => s.version}

#需要打包的文件
s.source_files = "LiaoLiaoCOSManager/*.{h,m}"

#为了同时支持真机和模拟器
s.pod_target_xcconfig = {'VALID_ARCHS' => 'x86_64 armv7 arm64'}

#依赖的库  这里自行填写，不要遗漏
s.frameworks   = 'UIKit', 'Foundation'

# 需要的其他三方库
s.dependency 'QCloudCOSXML/Transfer'


end

